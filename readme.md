### **Description**

Unlike Notion, Gutenberg does not have inbuilt todo checkbox list. This plugins adds a Gutenberg todo block titled "Gutenberg Todo List". This block enables you to add checkboxes with labels.

### High Level Description

This plugin was scaffolded using the `@wordpress/create-block` package. Out of the box, this gives you just one block. I restructured the plugin and created a separate blocks folder for all my blocks.

The plugin contains two blocks. The ToDo block List (which is a wrapper) and the ToDo Item.

The ToDo List is a parent block and can only contain one type of block. It uses `InnerBlocks` to include the ToDo Item.

The ToDo Item is a child of the ToDo List. It is not visible except only within the parent. It's made up of the `CheckboxControl` and `RichText`  components.

In the frontend, I hooked into the `render_block` hook to add `input type="checkbox"` to each of our lists.

The plugin generator does not contain any test. I included automated testing for each of the blocks. A test folder is found in each of the block folder.

### **Installation**

- Extract the plugin to your wp-content/plugins folder
- Go to Dashboard/Plugins and activate "**Todo Lists**" ****plugin

### **Getting Started**

To use the new block, create a new post, click on the plus icon and type "todo". You should now see the  "Gutenberg Todo List" block. Click on it and you should be able to add list items.

#### Here is a screenshot of what it looks like on the **backend**.

![Scheme](screenshots/backend.png)


#### Here is what it looks like on the **frontend**.

![Scheme](screenshots/frontend.png)

Important to note is the fact that the state of the checkbox is kept. If a checkbox is checked in the admin, it will be checked on the frontend as well.

### **Automated Testing**

For the purpose of this exercise, I decided to use End to End (e2e) testing to ensure the block works perfectly. This means I had to use `@wordpress/scripts` library which under the hood uses JEST and Puppeteer.

E2E testing also requires that we have a working WordPress Development environment running. If you are testing this plugin in a WordPress test website, you dont need to create another development site, just make sure you have a user with username-admin and password-password.

So, let's get started.

- Open your terminal and point it to the plugin folder
- Run `npm install --dev`
- Now run this command `npm run test:e2e -- --wordpress-base-url=http://wordpress.test/` . If your current local website is  for example devsite.local, please change it to `npm run test:e2e -- --wordpress-base-url=http://devsite.local/` . Lastly, make sure you have a user with username-admin and password-password on devsite.local
- Occassionally, you may see a test fail because of obsolete screenshot, please run `npm run test:e2e -- --wordpress-base-url=http://devsite.local/ -u` which will update the screenshots

#### Here is a video showing how this works

![Scheme](screenshots/jest.gif)

*** _To replay, kindly refresh the page or doenload jest.mp4 from the screenshots folder_

### **Translations**

This plugin is translation ready. Using WP CLI, I generated the .pot file and .json files which are ready to be translated to any other language. All translations are contained in the `languages` folder.

### Extensibility

As earlier stated, input checkbox is generated using the `render_block` hook. After the input html is generated, a filter is included for 3rd party developers to customize the input box as they wish.

### WordPress Coding Standard

WordPress coding standard was followed. The PHP code was formatted using PHPCS with the WordPress thandard.

### Compatibility/Trade-Offs

- Only works with WordPress Version 5.8 and above. Using block.json to create a block was only introduced in WP v5.8 and above
