import {
	createNewPost,
	enablePageDialogAccept,
	getEditedPostContent,
	insertBlock,
} from "@wordpress/e2e-test-utils";

describe("Gutenberg Todo Item", () => {
	beforeAll(async () => {
		enablePageDialogAccept();
	});
	beforeEach(async () => {
		await createNewPost();
	});

	test("if a new todo item is added when plus icon is clicked", async () => {
		await insertBlock("Gutenberg Todo List");

		const button = await page.$(
			".wp-block-david-towoju-todo-list .block-editor-button-block-appender"
		);
		await button.evaluate((button) => button.click());

		const items = (await page.$$('[data-type="david-towoju/todo-item"]'))
      .length;

		expect(items).toBe(2);

		expect(await getEditedPostContent()).toMatchSnapshot();
	});
});
