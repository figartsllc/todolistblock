/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from "@wordpress/i18n";
import { RichText, useBlockProps } from "@wordpress/block-editor";
import { CheckboxControl } from "@wordpress/components";
import { createBlock } from "@wordpress/blocks";
import "./editor.scss";

// Name of this block
const name = "david-towoju/todo-item";

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @return {WPElement} Element to render.
 */
export default function Edit({
	attributes,
	mergeBlocks,
	onReplace,
	setAttributes,
	style,
	clientId,
}) {
	const blockProps = useBlockProps({ style });

	function onChange(attribute) {
		return (newValue) => {
			setAttributes({ [attribute]: newValue });
		};
	}

	return (
		<>
			<div {...blockProps}>
				<CheckboxControl
					checked={!!attributes.checked}
					onChange={onChange("checked")}
					identifier="input"
				/>
				<RichText
					identifier="content"
					aria-label={__("Todo text", "todolists")}
					tagName="div"
					multiline={false}
					value={attributes.content}
					allowedFormats={["core/bold", "core/italic"]}
					onChange={(content) => setAttributes({ content })}
					onSplit={(value, isOriginal) => {
						let newAttributes;

						if (isOriginal || value) {
							newAttributes = {
								...attributes,
								content: value,
							};
						}

						const block = createBlock(name, newAttributes);

						if (isOriginal) {
							block.clientId = clientId;
						}

						return block;
					}}
					onMerge={mergeBlocks}
					onReplace={onReplace}
					onRemove={() => onReplace([])}
					placeholder={__("Add Item", "todolists")}
				></RichText>
			</div>
		</>
	);
}
