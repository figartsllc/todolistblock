/**
 * Registers a new block provided a unique name and an object defining its behavior.
 *
 */
import { __ } from "@wordpress/i18n";
import { registerBlockType } from "@wordpress/blocks";
import { InnerBlocks, useBlockProps } from "@wordpress/block-editor";

/**
 * Every block starts by registering a new block type definition.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-registration/
 */
registerBlockType("david-towoju/todo-list", {
	icon: {
		src: (
			<svg
				width="24"
				stroke-width="1.5"
				height="24"
				viewBox="0 0 24 24"
				fill="none"
				xmlns="http://www.w3.org/2000/svg"
			>
				<path
					d="M9 6L20 6"
					stroke="currentColor"
					stroke-linecap="round"
					stroke-linejoin="round"
				/>
				<path
					d="M3.80002 5.79999L4.60002 6.59998L6.60001 4.59999"
					stroke="currentColor"
					stroke-linecap="round"
					stroke-linejoin="round"
				/>
				<path
					d="M3.80002 11.8L4.60002 12.6L6.60001 10.6"
					stroke="currentColor"
					stroke-linecap="round"
					stroke-linejoin="round"
				/>
				<path
					d="M3.80002 17.8L4.60002 18.6L6.60001 16.6"
					stroke="currentColor"
					stroke-linecap="round"
					stroke-linejoin="round"
				/>
				<path
					d="M9 12L20 12"
					stroke="currentColor"
					stroke-linecap="round"
					stroke-linejoin="round"
				/>
				<path
					d="M9 18L20 18"
					stroke="currentColor"
					stroke-linecap="round"
					stroke-linejoin="round"
				/>
			</svg>
		),
	},
	edit: () => {
		const blockProps = useBlockProps();

		return (
			<div {...blockProps}>
				<InnerBlocks
					allowedBlocks={["david-towoju/todo-item"]}
					template={[
						[
							"david-towoju/todo-item",
							{ placeholder: __("Add Item", "todolists") },
						],
					]}
					templateInsertUpdatesSelection={true}
				/>
			</div>
		);
	},

	save: () => {
		const blockProps = useBlockProps.save();

		return (
			<div {...blockProps}>
				<InnerBlocks.Content />
			</div>
		);
	},
});
