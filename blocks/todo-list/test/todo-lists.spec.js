import {
	createNewPost,
	enablePageDialogAccept,
	getEditedPostContent,
	insertBlock,
} from "@wordpress/e2e-test-utils";

describe("Gutenberg Todo List", () => {
	beforeAll(async () => {
		enablePageDialogAccept();
	});
	beforeEach(async () => {
		await createNewPost();
	});

	test("Todo Lists block should be available", async () => {
    await insertBlock("Gutenberg Todo List");

		// Check if block was inserted
		expect(
			await page.$('[data-type="david-towoju/todo-list"]')
    ).not.toBeNull();

		expect(await getEditedPostContent()).toMatchSnapshot();
	});

});
