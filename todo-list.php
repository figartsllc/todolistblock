<?php
/**
 * The plugin bootstrap file
 *
 * @link    https://figarts.co/david
 * @since   1.0
 * @package todolists
 *
 * @wordpress-plugin
 * Plugin Name:       Todo Lists
 * Description:       Todo Lists for WordPress Gutenberg.
 * Requires at least: 5.8
 * Requires PHP:      7.0
 * Version:           1.0.0
 * Author:            David Towoju
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       todolists
 */

// Load the core plugin class that contains all hooks.
require plugin_dir_path( __FILE__ ) . 'class-todolist.php';
$todo = new ToDoList();
$todo->run_hooks();
